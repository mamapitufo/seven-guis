(ns timer.app
  (:require
   [reagent.dom :as rdom]
   [timer.events :as events]
   [timer.views :as views]))

(defn app []
  [:div.app {:style {:width 350}}
   [:header [:h1.title "Timer"]]
   [:main.content
    [views/elapsed-progress]
    [views/elapsed-time]
    [views/duration-slider]
    [views/reset-button]]])

(defn ^:dev/after-load start []
  (events/reset)
  (rdom/render [app]
               (.getElementById js/document "app")))

(defn ^:export init []
  (js/console.log "[init]")
  (start))

(defn ^:dev/before-load stop []
  )

