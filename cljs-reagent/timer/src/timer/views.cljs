(ns timer.views
  (:require
   [clojure.pprint :refer [cl-format]]
   [timer.db :as db]
   [timer.events :as events]))

(defn elapsed-time
  []
  [:div (cl-format nil "~,1Fs" (/ (:elapsed @db/state) 1000))])

(defn elapsed-progress
  []
  (let [{:keys [elapsed duration]} @db/state]
    [:div.elapsed-progress
     [:label {:for "progress"} "Elapsed Time:"]
     [:progress {:id "progress"
                 :max duration
                 :value elapsed}]]))

(defn- duration-change-handler
  [e]
  (events/update-duration (-> e .-target .-value)))

(defn duration-slider
  []
  (let [duration (:duration @db/state)]
    [:div.duration-slider
     [:label {:for "duration"} "Duration:"]
     [:input.range {:type "range"
                    :id "duration"
                    :min 0 :max 29000
                    :step 100
                    :value duration
                    :on-change duration-change-handler}]]))

(defn reset-button
  []
  [:input.reset-btn {:type "button"
                     :on-click events/reset
                     :value "Reset"}])
