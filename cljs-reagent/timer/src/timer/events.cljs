(ns timer.events
  (:require
   [timer.db :as db]))

(def ^:private interval-ms 100)

(defn- running?
  []
  (:running? @db/state))

(defn- toggle-running
  [running?]
  (swap! db/state assoc :running? running?))

(defn- increment-elapsed
  "Increments the elapsed seconds value in the state"
  []
  (swap! db/state update :elapsed (partial + interval-ms)))

(defn- tick
  "Schedules a timer tick, if necessary."
  []
  (let [{:keys [duration elapsed running?]} @db/state]
    (if (and running?
             (< elapsed duration))
      (do
        (increment-elapsed)
        (js/console.debug "Scheduling next tick: " (:elapsed @db/state))
        (js/setTimeout tick interval-ms))
      (do
        (toggle-running false)
        (js/console.debug "Stopping timer")))))

(defn start
  "Starts the timer. If it is already running it does nothing."
  []
  (when-not (running?)
    (js/console.debug "Starting timer")
    (toggle-running true)
    (tick)))

(defn reset
  "Resets the elapsed time to 0."
  []
  (js/console.debug "Resetting timer to 0")
  (swap! db/state assoc :elapsed 0)
  (start))

(defn update-duration
  "Updates the :duration value, in ms., in the state."
  [ms]
  (swap! db/state assoc :duration ms)
  (start))
