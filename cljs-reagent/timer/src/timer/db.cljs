(ns timer.db
  (:require
    [reagent.core :as r]))

(defonce state (r/atom {:elapsed 0
                        :duration 13000
                        :running? false}))
