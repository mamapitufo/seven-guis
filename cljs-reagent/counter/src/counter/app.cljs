(ns counter.app
  (:require [reagent.dom :as rdom]
            [reagent.core :as r]))

(defn counter []
  (let [count (r/atom 0)]
    (fn []
      [:div.counter
       [:span.label @count]
       [:input.count-btn {:type "button"
                          :value "Count"
                          :on-click #(swap! count inc)}]])))

(defn app []
  [:div.app
   [:header [:h1.title "Counter"]]
   [:div.content
    [counter]]])

(defn ^:dev/after-load start []
  (rdom/render [app]
               (.getElementById js/document "app")))

(defn ^:export init []
  (js/console.log "[init]")
  (start))

(defn ^:dev/before-load stop [])

