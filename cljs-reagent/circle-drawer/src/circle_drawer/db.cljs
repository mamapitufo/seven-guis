(ns circle-drawer.db
  (:require
   [reagent.core :as r]))

(defonce state (r/atom {:circles {}
                        :selected-circle nil}))
