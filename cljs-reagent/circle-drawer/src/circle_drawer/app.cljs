(ns circle-drawer.app
  (:require
   [circle-drawer.views :as views]
   [reagent.dom :as rdom]))

(defn app []
  [:div.app {:style {:width 800}}
   [:header [:h1.title "Circle Drawer"]]
   [:main.content
    [views/controls]
    [views/drawing-surface]]])

(defn ^:dev/after-load start []
  (rdom/render [app]
               (.getElementById js/document "app")))

(defn ^:export init []
  (js/console.log "[init]")
  (start))

(defn ^:dev/before-load stop []
  )

