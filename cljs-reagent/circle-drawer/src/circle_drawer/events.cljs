(ns circle-drawer.events
  (:require
   [circle-drawer.db :as db]
   [circle-drawer.utils :as utils]))

(defonce default-r 23)

(defn- add-circle
  [[x y]]
  (let [id (random-uuid)]
    (swap! db/state assoc-in [:circles id] {:id id
                                            :x x :y y
                                            :r default-r})))

(defn- select-circle
  [id]
  (swap! db/state assoc :selected-circle id))

(defn add-or-select-circle
  [point]
  (if-let [clicked-circle (utils/circle-under-point? point)]
    (select-circle clicked-circle)
    (do
      (select-circle nil)
      (add-circle point))))

(comment

  (cljs.pprint/pprint @db/state)
  (:selected-circle @db/state)

  (do
    (add-circle [100 100])
    (add-circle [50 200])))
