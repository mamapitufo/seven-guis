(ns circle-drawer.utils
  (:require
   [circle-drawer.db :as db]))

(defn- inside-circle?
  [[xp yp] {:keys [x y r ] :as circle}]
  (let [distance-sq (+ (js/Math.pow (- xp x) 2)
                       (js/Math.pow (- yp y) 2))
        r-sq (js/Math.pow r 2)]
    (if (<= distance-sq r-sq)
      circle
      false)))

(defn circle-under-point?
  [[x y]]
  (let [circles (-> @db/state :circles vals)
        clicked-circle (some (partial inside-circle? [x y]) circles)]
    (when clicked-circle
      (:id clicked-circle))))
