(ns circle-drawer.views
  (:require
   [circle-drawer.db :as db]
   [circle-drawer.events :as events]))

(defn undo-btn
  []
  [:input.btn {:type "button"
               :value "Undo"
               :disabled true}])

(defn redo-btn
  []
  [:input.btn {:type "button"
               :value "Redo"
               :disabled true}])

(defn controls
  []
  [:div.controls
   (undo-btn)
   (redo-btn)])

(defn- draw-circles [canvas state]
  (when canvas
    (let [circles (-> state :circles vals)
          selected-circle (:selected-circle state)
          ctx (.getContext canvas "2d")
          height (.-height canvas)
          width (.-width canvas)]
      (set! (.-fillStyle ctx) "white")
      (.fillRect ctx 0 0 width height)
      (doseq [{:keys [x y r id]} circles]
        (.beginPath ctx)
        (if (= id selected-circle)
          (set! (.-fillStyle ctx) "lightgrey")
          (set! (.-fillStyle ctx) "white"))
        (.arc ctx x y r 0 (* 2 js/Math.PI) false)
        (.stroke ctx)
        (.fill ctx)))))

(defn drawing-surface
  []
  (let [!canvas (atom nil)]
    (letfn [(set-canvas-ref [el]
              (reset! !canvas el)
              (draw-circles @!canvas @db/state))
            (handle-click [e]
              (let [x (-> e .-nativeEvent .-offsetX)
                    y (-> e .-nativeEvent .-offsetY)]
                (events/add-or-select-circle [x y])))]
      (fn []
        (draw-circles @!canvas @db/state)
        [:canvas.drawing-surface {:width 774
                                  :height 387
                                  :ref set-canvas-ref
                                  :on-click handle-click}]))))

