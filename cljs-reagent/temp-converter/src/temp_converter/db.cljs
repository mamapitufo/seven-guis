(ns temp-converter.db
  (:require [reagent.core :as r]))

(defonce state (r/atom {:fahrenheit {:value "" :error nil}
                        :celsius {:value "" :error nil}}))
