(ns temp-converter.views
  (:require [temp-converter.db :as db]
            [temp-converter.events :as events]))

(defn- temp-input
  [t-key label update-event]
  (letfn [(update-temp [e]
            (update-event (-> e .-target .-value)))]
    (fn []
      (let [{:keys [value error]} (t-key @db/state)]
        [:div.temp-input
         [:input {:id t-key
                  :type "text"
                  :value value
                  :class (when error "invalid")
                  :title error
                  :on-change update-temp}]
         [:label {:for t-key} label]]))))

(def fahrenheit-input
  (temp-input :fahrenheit "Fahrenheit" events/update-fahrenheit))

(def celsius-input
  (temp-input :celsius "Celsius" events/update-celsius))

(defn converter-ui []
  [:div.converter
   [celsius-input]
   [:span "="]
   [fahrenheit-input]])
