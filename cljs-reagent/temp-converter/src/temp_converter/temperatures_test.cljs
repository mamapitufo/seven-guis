(ns temp-converter.temperatures-test
  (:require [cljs.test :refer [deftest is]]
            [temp-converter.temperatures :refer [fahrenheit->celsius
                                                 celsius->fahrenheit
                                                 valid-temperature?]]))

(deftest fahrenheit
  (is (= "0.00" (fahrenheit->celsius 32)))
  (is (= "37.78" (fahrenheit->celsius "100"))))

(deftest invalid-fahrenheit
  (is (nil? (fahrenheit->celsius nil)))
  (is (nil? (fahrenheit->celsius "")))
  (is (nil? (fahrenheit->celsius "not a temperature"))))

(deftest celsius
  (is (= "-34.60" (celsius->fahrenheit "-37")))
  (is (= "212.00" (celsius->fahrenheit 100))))

(deftest invalid-celsius
  (is (nil? (celsius->fahrenheit nil)))
  (is (nil? (celsius->fahrenheit "  ")))
  (is (nil? (celsius->fahrenheit ##NaN))))

(deftest valid-temperatures
  (is (true? (valid-temperature? "123.45")))
  (is (true? (valid-temperature? -12)))
  (is (true? (valid-temperature? "+97 ")))
  (is (true? (valid-temperature? "      -23     "))))

(deftest invalid-temperatures
  (is (false? (valid-temperature? "123.45.")))
  (is (false? (valid-temperature? ".67")))
  (is (false? (valid-temperature? "")))
  (is (false? (valid-temperature? "    a5"))))
