(ns temp-converter.temperatures
  (:require [clojure.string :as string]))

(defn celsius->fahrenheit
  "Converts temperature t in degrees Celsius to Fahrenheit"
  [t]
  (let [t (js/parseFloat t)]
    (when (not (js/isNaN t))
      (-> t
          (* (/ 9.0 5))
          (+ 32)
          (.toFixed 2)))))

(defn fahrenheit->celsius
  "Converts temperature t in degrees Fahrenheit to Celsius"
  [t]
  (let [t (js/parseFloat t)]
    (when (not (js/isNaN t))
      (-> t
          (- 32)
          (* (/ 5.0 9))
          (.toFixed 2)))))

(defn valid-temperature?
  "Return true if t is a valid temperature"
  [t]
  (->> t
       .toString
       string/trim
       (re-matches #"[-+]?\d+(\.\d+)?")
       boolean))

