(ns temp-converter.events
  (:require [clojure.string :as string]
            [temp-converter.db :as db]
            [temp-converter.temperatures :as tt]))

(defn- update-temperatures
  "Updates the value of the temperature k in the state. If the temperature is valid the
  dependent temperature will be updated as well, using the conversion function."
  [k v]
  (let [dependent (if (= k :fahrenheit) :celsius :fahrenheit)
        convert-fn (if (= k :fahrenheit) tt/fahrenheit->celsius tt/celsius->fahrenheit)
        value (string/trim v)
        valid? (tt/valid-temperature? value)
        error (when-not valid? "Invalid temperature")]
    (swap! db/state (fn [state]
                      (-> state
                          (assoc k {:value value :error error})
                          (conj (when valid? [dependent {:value (convert-fn value)
                                                         :error nil}])))))))

(defn update-fahrenheit
  "Updates the value of `fahrenheit` in the state. If t is a valid temperature
  the value of `celsius` will be calculated and updated as well."
  [t]
  (update-temperatures :fahrenheit t))

(defn update-celsius
  "Updates the value of `celsius` in the state. If it is a valid temperature
  the value of `fahrenheit` will be calculated and updated as well."
  [t]
  (update-temperatures :celsius t))

