(ns temp-converter.app
  (:require [reagent.dom :as rdom]
            [temp-converter.views :as views]))

(defn app []
  [:div.app {:style {:width 600}}
   [:header
    [:h1.title "Temperature Converter"]]
   [:main.content
    [views/converter-ui]]])

(defn ^:dev/after-load start []
  (rdom/render [app]
               (.getElementById js/document "app")))

(defn ^:export init []
  (js/console.log "[init]")
  (start))

(defn ^:dev/before-load stop []
  )

