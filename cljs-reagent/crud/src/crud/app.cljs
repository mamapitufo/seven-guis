(ns crud.app
  (:require
   [crud.views :as views]
   [reagent.dom :as rdom]))

(defn app []
  [:div.app {:style {:width 480}}
   [:header [:h1.title "CRUD"]]
   [:main.content
    (views/filter-prefix-input)
    [:div.list-detail
     (views/people-list)
     [:div
      (views/forename-input)
      (views/surname-input)]]
    (views/controls)]])

(defn ^:dev/after-load start []
  (rdom/render [app]
               (.getElementById js/document "app")))

(defn ^:export init []
  (js/console.log "[init]")
  (start))

(defn ^:dev/before-load stop []
  )

