(ns crud.db
  (:require
   [reagent.core :as r]))

(defonce state (r/atom {:people {}
                        :person-detail {:id js/undefined
                                        :forename ""
                                        :surname ""}
                        :filter-prefix ""}))

(defn people
  "List of people whose surname starts with the filter prefix"
  []
  (let [{:keys [filter-prefix people]} @state]
    (if (empty? filter-prefix)
      (vals people)
      (filter (fn [person]
                (.startsWith (:surname person) filter-prefix))
              (vals people)))))

