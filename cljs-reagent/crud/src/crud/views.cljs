(ns crud.views
  (:require
   [clojure.string :as s]
   [crud.db :as db]
   [crud.events :as events]))

(defn- person-entry
  [{:keys [id forename surname]}]
  [:option {:key id
            :value id}
   (s/join ", " [surname forename])])

(defn- event-val
  [e]
  (-> e .-target .-value))

(defn people-list
  "Displays the list of people in the database"
  []
  [:select.people {:size 4
                   :value (get-in @db/state [:person-detail :id])
                   :on-change #(events/load-person-details (keyword (event-val %)))}
   (map person-entry (db/people))])

(defn- input
  ([id label value update-fn class]
   [:div.input {:class (when (some? class) class)}
    [:label {:for id} label]
    [:input {:type "text"
             :id id
             :value value
             :on-change update-fn}]])
  ([id label value update-fn]
   (input id label value update-fn nil)))

(defn filter-prefix-input
  "Updates the filter prefix in the state"
  []
  (input :filter-prefix
         "Filter prefix:"
         (:filter-prefix @db/state)
         #(events/update-filter-prefix (event-val %))
         "filter-prefix"))

(defn forename-input
  []
  (input :forename
         "Name:"
         (get-in @db/state [:person-detail :forename])
         #(events/update-forename (-> % .-target .-value))
         "detail-input"))

(defn surname-input
  []
  (input :surname
         "Surname:"
         (get-in @db/state [:person-detail :surname])
         #(events/update-surname (event-val %))
         "detail-input"))

(defn- create-btn
  []
  (let [{:keys [forename surname]} (:person-detail @db/state)]
    [:input {:type "button"
             :value "Create"
             :disabled (every? empty? [forename surname])
             :on-click events/create-person}]))

(defn- update-btn
  []
  (let [{:keys [id] :as person} (:person-detail @db/state)
        original-person (get-in @db/state [:people id])]
    [:input {:type "button"
             :value "Update"
             :disabled (or (nil? id) (= person original-person))
             :on-click events/update-person}]))

(defn- delete-btn
  []
  (let [id (get-in @db/state [:person-detail :id])]
    [:input {:type "button"
             :value "Delete"
             :disabled (nil? id)
             :on-click events/delete-person}]))

(defn controls
  []
  [:div.controls
   (create-btn)
   (update-btn)
   (delete-btn)])

