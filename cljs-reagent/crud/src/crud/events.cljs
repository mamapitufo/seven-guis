(ns crud.events
  (:require
   [crud.db :as db]))

(defn- reset-person-detail
  []
  (swap! db/state assoc :person-detail {:id js/undefined
                                        :forename ""
                                        :surname ""}))

(defn add-person
  "Adds a new entry to the `people` database"
  [forename surname]
  (let [id (-> (random-uuid) .toString keyword)]
    (swap! db/state assoc-in [:people id] {:id id
                                           :forename forename
                                           :surname surname})))

(defn create-person
  "Creates a person from the data in the detail view"
  []
  (let [{:keys [forename surname]} (:person-detail @db/state)]
    (add-person forename surname)
    (reset-person-detail)))

(defn update-person
  "Updates the people database with the current values in the detail view"
  []
  (let [{:keys [id] :as person} (:person-detail @db/state)]
    (swap! db/state assoc-in [:people id] person)
    (reset-person-detail)))

(defn delete-person
  "Deletes the selected person from the people database"
  []
  (let [id (get-in @db/state [:person-detail :id])]
    (swap! db/state update-in [:people] dissoc id)
    (reset-person-detail)))

(defn update-filter-prefix
  "Updates the filter prefix for the list of people"
  [filter-prefix]
  (swap! db/state assoc :filter-prefix filter-prefix)
  (reset-person-detail))

(defn load-person-details
  "Loads a new person in the detail view"
  [id]
  (let [person (get-in @db/state [:people id] {})]
    (swap! db/state assoc :person-detail person)))

(defn update-forename
  "Updates the forename in the detail view"
  [forename]
  (swap! db/state assoc-in [:person-detail :forename] forename))

(defn update-surname
  "Updates the surname in the detail view"
  [surname]
  (swap! db/state assoc-in [:person-detail :surname] surname))

