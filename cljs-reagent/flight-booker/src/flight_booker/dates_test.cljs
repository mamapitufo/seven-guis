(ns flight-booker.dates-test
  (:require
   [cljs.test :refer [deftest is]]
   [flight-booker.dates :refer [format-short-date parse-date valid-date?]]))

(deftest valid-dates
  (is (true? (valid-date? (js/Date.))))
  (is (true? (valid-date? (js/Date. "02.04.2022")))))

(deftest invalid-dates
  (is (false? (valid-date? (js/Date. ""))))
  (is (false? (valid-date? (js/Date. "rubbish"))))
  (is (false? (valid-date? ##NaN)))
  (is (false? (valid-date? 7)))
  (is (false? (valid-date? nil)))
  (is (false? (valid-date? "12-12-2020"))))

(deftest parse-full-date
  (is (= #inst "2022-04-06T11:00:00.000-00:00" (parse-date "06/04/2022")))
  (is (= #inst "2022-04-06T11:00:00.000-00:00" (parse-date "06-04-2022")))
  (is (= #inst "2022-04-06T11:00:00.000-00:00" (parse-date "06.04.2022"))))

(deftest parse-abbreviated-date
  (is (= #inst "2022-04-06T11:00:00.000-00:00" (parse-date "6/4/2022")))
  (is (= #inst "2022-04-06T11:00:00.000-00:00" (parse-date "6-4-2022"))))

(deftest parse-invalid-date
  (is (nil? (parse-date "28-13-2022")))
  (is (nil? (parse-date "32-4-22")))
  (is (nil? (parse-date "12/04/22")))
  (is (nil? (parse-date "")))
  (is (nil? (parse-date "rubbish")))
  (is (nil? (parse-date "12 06 1984"))))

(deftest format-valid-date
  (is (= "06/04/2022" (format-short-date (js/Date. "04/06/2022")))))

(deftest format-invalid-date
  (is (nil? (format-short-date (js/Date. ""))))
  (is (nil? (format-short-date nil))))
