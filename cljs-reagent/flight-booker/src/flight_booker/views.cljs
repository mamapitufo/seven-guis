(ns flight-booker.views
  (:require
   [flight-booker.db :as db]
   [flight-booker.events :as events]))

(defn- flight-type-change-handler
  [e]
  (let [selected (-> e .-target .-value)]
    (js/console.log selected)
    (events/update-flight-type (keyword selected))))

(defn flight-type-chooser
  "UI to toggle between one-way and return flight."
  []
  (let [flight-type (name (:flight-type @db/state))]
    [:select.flight-type-chooser {:on-change flight-type-change-handler
                                  :value flight-type}

     [:option {:value "one-way"} "One-way flight"]
     [:option {:value "return"} "Return flight"]]))

(defn- date-input
  ([leg change-fn]
   (date-input leg change-fn false))
  ([leg change-fn disabled]
   {:pre [(some? leg) (fn? change-fn)]}
   (let [{:keys [value valid? error]} (leg @db/state)
         invalid? (and (not valid?) (not= "" value))
         error (if (some? error) error "")]
     [:input.date-input {:type "text"
                         :value value
                         :disabled disabled
                         :class (when invalid? "invalid")
                         :title error
                         :on-change change-fn}])))

(defn departure-input
  "UI for entering and displaying the departure date."
  []
  (letfn [(update-departure [e]
            (events/update-departure (-> e .-target .-value)))]
    (date-input :departure update-departure)))

(defn return-input
  "UI for entering and displaying the return date."
  []
  (letfn [(update-return [e]
            (events/update-return (-> e .-target .-value)))]
    (date-input :return update-return (= :one-way (:flight-type @db/state)))))

(defn- book-flight
  []
  (let [{:keys [flight-type departure return]} @db/state
        message (if (= flight-type :one-way)
                  (str "You have booked a one-way flight on " (:value departure) ".")
                  (str "You have booked a return flight from "
                       (:value departure) " to " (:value return) "."))]
    (js/alert message)))

(defn book-button
  "UI for booking a flight with the current details."
  []
  [:input.book-btn {:type "button"
                    :value "Book"
                    :disabled (not (:bookable? @db/state))
                    :on-click book-flight}])
