(ns flight-booker.dates
  (:require
   [cljs.pprint :refer [cl-format]]
   [clojure.string :as string]))

(defn valid-date?
  "Returns true if date is a valid date instance."
  [date]
  (boolean (and (not (nil? date))
                (.-getDate date)
                (not (js/isNaN (.getDate date))))))

(defn parse-date
  "Parses a user entered string into a js/Date object. Returns nil if the date
  is invalid. Accepts input similar to `new Date(s)`, but with day, month, year
  instead."
  [input]
  {:pre [(string? input)]}
  (let [input (string/trim input)
        [_ day month year] (re-find #"^(\d{1,2})[./-](\d{1,2})[./-](\d{4})$" input)
        date (js/Date. (str year "/" month "/" day " 12:00:00"))]
    (when (valid-date? date) date)))

(defn format-short-date
  "Formats a js/Date as 'dd/mm/yyyy'. Returns nil if date is invalid."
  [date]
  (when (valid-date? date)
    (cl-format nil "~2,'0D/~2,'0D/~4,'0D"
               (.getDate date)
               (inc (.getMonth date))
               (.getFullYear date))))
