(ns flight-booker.events-test
  (:require
   [cljs.test :refer [deftest is]]
   [flight-booker.db :as db]
   [flight-booker.events :as events]
   [reagent.core :as r]))

(deftest valid-departure-date-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (is (= {:value "13.12.2011" :valid? true :epoch 1323777600000 :error nil}
           (:departure (events/update-departure "13.12.2011"))))))

(deftest multiple-departure-updates-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (is (= {:value "13/12/2011" :valid? true :epoch 1323777600000 :error nil}
           (:departure (events/update-departure "13/12/2011"))))
    (is (= {:value "19/04/2022" :valid? true :epoch 1650366000000 :error nil}
           (:departure (events/update-departure "19/04/2022"))))))

(deftest invalid-departure-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (let [{:keys [error epoch valid?]} (:departure (events/update-departure "03/15/2022"))]
      (is (some? error))
      (is (nil? epoch))
      (is (false? valid?)))))

(deftest valid-date-clears-errors-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (let [first-res (:departure (events/update-departure "03/15/2022"))
          second-res (:departure (events/update-departure "01/01/1970"))]
      (is (some? (:error first-res)))
      (is (false? (:valid? first-res)))

      (is (nil? (:error second-res)))
      (is (true? (:valid? second-res))))))

(deftest valid-return-date-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (is (= {:value "13/12/2011" :valid? true :epoch 1323777600000 :error nil}
           (:return (events/update-return "13/12/2011"))))))

(deftest invalid-return-date-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (let [{:keys [error valid? epoch]} (:return (events/update-return "03/15/2022"))]
      (is (some? error))
      (is (nil? epoch))
      (is (false? valid?)))))

(deftest valid-one-way-flight-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (is (false? (:bookable? @db/state)))
    (is (true? (:bookable? (events/update-departure "13/12/2011"))))))

(deftest valid-return-flight-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (is (true? (:bookable? (events/update-departure "13/12/2011"))))
    (is (false? (:bookable? (events/update-flight-type :return))))
    (is (true? (:bookable? (events/update-return "15/03/2022"))))))

(deftest invalid-one-way-fligh-bad-date-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (is (false? (:bookable? (events/update-departure "invalid date"))))))

(deftest invalid-two-way-fligh-bad-date-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (events/update-departure "13/12/2011")
    (events/update-flight-type :return)
    (is (false? (:bookable? (events/update-return "11"))))))

(deftest invalid-two-way-flight-return-too-early-test
  (with-redefs [db/state (r/atom db/initial-state)]
    (events/update-departure "13/12/2011")
    (events/update-flight-type :return)
    (is (false? (:bookable? (events/update-return "11/12/2011"))))))
