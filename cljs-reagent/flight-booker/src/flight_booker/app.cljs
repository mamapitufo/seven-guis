(ns flight-booker.app
  (:require
   [flight-booker.dates :as dates]
   [flight-booker.events :as events]
   [flight-booker.views :as views]
   [reagent.dom :as rdom]))

(defn app []
  [:div.app {:style {:width 230}}
   [:header [:h1.title "Flight Booker"]]
   [:main.content
    [views/flight-type-chooser]
    [views/departure-input]
    [views/return-input]
    [views/book-button]]])

(defn ^:dev/after-load start []
  (rdom/render [app]
               (.getElementById js/document "app")))

(defn ^:export init []
  (js/console.log "[init]")
  (let [now (dates/format-short-date (js/Date.))]
    (events/update-departure now)
    (events/update-return now))
  (start))

(defn ^:dev/before-load stop []
  )

