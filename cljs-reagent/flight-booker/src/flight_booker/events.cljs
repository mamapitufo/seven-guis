(ns flight-booker.events
  (:require
   [flight-booker.dates :as dates]
   [flight-booker.db :as db]))

(defn- bookable?
  "Determines whether a flight can be booked or not."
  [state]
  (let [{:keys [departure return flight-type]} state]
    (condp = flight-type
      :one-way (:valid? departure)
      :return (and (:valid? departure)
                   (:valid? return)
                   (<= (:epoch departure) (:epoch return))))))

(defn- update-leg
  "Updates the date specified by leg and the `bookable?` constraint on the state."
  [leg value]
  {:pre [(#{:departure :return} leg) (string? value)]}
  (let [date (dates/parse-date value)
        valid? (dates/valid-date? date)
        epoch (when valid? (.getTime date))
        error (when-not valid? "Please enter a valid date")]
    (swap! db/state (fn [state]
                      (as-> state $
                        (assoc $ leg {:value value
                                      :valid? valid?
                                      :epoch epoch
                                      :error error})
                        (assoc $ :bookable? (bookable? $)))))))

(defn update-departure
  "Updates the departure date and the `bookable?` constraint on the state."
  [value]
  (update-leg :departure value))

(defn update-return
  "Updates the return date and the `bookable?` constraint on the state."
  [value]
  (update-leg :return value))

(defn update-flight-type
  "Updates the flight type to either `:one-way` or `:return`. It also
  updates the `bookable?` constraint."
  [flight-type]
  {:pre [(#{:one-way :return} flight-type)]}
  (swap! db/state (fn [state]
                    (as-> state $
                      (assoc $ :flight-type flight-type)
                      (assoc $ :bookable? (bookable? $))))))
