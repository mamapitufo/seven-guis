(ns flight-booker.db
  (:require [reagent.core :as r]))

(defonce default-date {:value ""
                       :valid? false
                       :epoch nil
                       :error :nil})

(defonce initial-state {:flight-type :one-way
                        :departure default-date
                        :return default-date
                        :bookable? false})

(defonce state (r/atom initial-state))
